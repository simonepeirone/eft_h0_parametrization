!----------------------------------------------------------------------------------------
!
! This file is part of EFTCAMB.
!
! Copyright (C) 2013-2017 by the EFTCAMB authors
!
! The EFTCAMB code is free software;
! You can use it, redistribute it, and/or modify it under the terms
! of the GNU General Public License as published by the Free Software Foundation;
! either version 3 of the License, or (at your option) any later version.
! The full text of the license can be found in the file eftcamb/LICENSE at
! the top level of the EFTCAMB distribution.
!
!----------------------------------------------------------------------------------------

!> @file 04p8_taylor_expansion_parametrizations_1D.f90
!! This file contains the definition of the Taylor expansion parametrization, around a=0,
!! up to third order, inheriting from parametrized_function_1D.


!----------------------------------------------------------------------------------------
!> This module contains the definition of the Taylor expansion parametrization, around a=0,
!! up to third order, inheriting from parametrized_function_1D.

!> @author Simone Peirone

module EFTCAMB_H0_parametrizations_1D

    use precision
    use AMLutils
    use EFT_def
    use EFTCAMB_cache
    use EFTCAMB_abstract_parametrizations_1D

    implicit none

    private

    public H0_parametrization_1D

    ! ---------------------------------------------------------------------------------------------
    !> Type containing the Taylor expansion parametrization. Inherits from parametrized_function_1D.
    type, extends ( parametrized_function_1D ) :: H0_parametrization_1D

        real(dl) :: alpha
        real(dl) :: b
        real(dl) :: c

    contains

        ! utility functions:
        procedure :: set_param_number      => H0Parametrized1DSetParamNumber      !< subroutine that sets the number of parameters of the Taylor expansion parametrized function.
        procedure :: init_parameters       => H0Parametrized1DInitParams          !< subroutine that initializes the function parameters based on the values found in an input array.
        procedure :: parameter_value       => H0Parametrized1DParameterValues     !< subroutine that returns the value of the function i-th parameter.
        procedure :: feedback              => H0Parametrized1DFeedback            !< subroutine that prints to screen the informations about the function.

        ! evaluation procedures:
        procedure :: value                 => H0Parametrized1DValue               !< function that returns the value of the Taylor expansion.
        procedure :: first_derivative      => H0Parametrized1DFirstDerivative     !< function that returns the first derivative of the Taylor expansion.
        procedure :: second_derivative     => H0Parametrized1DSecondDerivative    !< function that returns the second derivative of the Taylor expansion.
        procedure :: third_derivative      => H0Parametrized1DThirdDerivative     !< function that returns the third derivative of the Taylor expansion.
        procedure :: integral              => H0Parametrized1DIntegral            !< function that returns the strange integral that we need for w_DE.

    end type H0_parametrization_1D

contains

    ! ---------------------------------------------------------------------------------------------
    ! Implementation of the H0 expansion parametrization.
    ! ---------------------------------------------------------------------------------------------

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that sets the number of parameters of the H0 expansion parametrized function.
    subroutine H0Parametrized1DSetParamNumber( self )

        implicit none

        class(H0_parametrization_1D) :: self       !< the base class

        ! initialize the number of parameters:
        self%parameter_number = 3

    end subroutine H0Parametrized1DSetParamNumber

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that initializes the function parameters based on the values found in an input array.
    subroutine H0Parametrized1DInitParams( self, array )

        implicit none

        class(H0_parametrization_1D)                            :: self   !< the base class.
        real(dl), dimension(self%parameter_number), intent(in)  :: array  !< input array with the values of the parameters.

        self%alpha  = array(1)
        self%b      = array(2)
        self%c      = array(3)

    end subroutine H0Parametrized1DInitParams

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that returns the value of the function i-th parameter.
    subroutine H0Parametrized1DParameterValues( self, i, value )

        implicit none

        class(H0_parametrization_1D)           :: self        !< the base class
        integer     , intent(in)               :: i           !< The index of the parameter
        real(dl)    , intent(out)              :: value       !< the output value of the i-th parameter

        select case (i)
            case(1)
                value = self%alpha
            case(2)
                value = self%b
            case(3)
                value = self%c
            case default
                write(*,*) 'Illegal index for parameter_names.'
                write(*,*) 'Maximum value is:', self%parameter_number
                call MpiStop('EFTCAMB error')
        end select

    end subroutine H0Parametrized1DParameterValues

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that prints to screen the informations about the function.
    subroutine H0Parametrized1DFeedback( self, print_params )

        implicit none

        class(H0_parametrization_1D)     :: self         !< the base class
        logical, optional                :: print_params !< optional flag that decised whether to print numerical values
                                                         !! of the parameters.

        integer                                 :: i
        real(dl)                                :: param_value
        character(len=EFT_names_max_length)     :: param_name
        logical                                 :: print_params_temp

        if ( present(print_params) ) then
            print_params_temp = print_params
        else
            print_params_temp = .True.
        end if

        if ( print_params_temp ) then
          write(*,*)     'H0 parametrization for wDE: ', self%name
            do i=1, self%parameter_number
                call self%parameter_names( i, param_name  )
                call self%parameter_value( i, param_value )
                write(*,'(a23,a,F12.6)') param_name, '=', param_value
            end do
        end if

    end subroutine H0Parametrized1DFeedback

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the value of the function in the scale factor.
    function H0Parametrized1DValue( self, x, eft_cache, eft_par_cache )

        implicit none

        class(H0_parametrization_1D)                       :: self      !< the base class
        real(dl), intent(in)                               :: x         !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(in), optional:: eft_par_cache !< the optional input EFTCAMB cache
        type(EFTCAMB_timestep_cache), intent(in), optional :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: H0Parametrized1DValue                               !< the output value

        real(dl) :: b,c,al,grhom_tot,gpim_tot,h0,a

        a =x
        b = self%b
        c = self%c
        al = self%alpha
        h0  = eft_par_cache%h0_Mpc

        grhom_tot = eft_cache%grhob_t+eft_cache%grhoc_t+eft_cache%grhor_t+eft_cache%grhog_t +eft_cache%grhonu_tot
        gpim_tot  = 1._dl/3._dl*(eft_cache%grhor_t+eft_cache%grhog_t) +eft_cache%gpinu_tot
        if (x<1.E-5_dl) then
          H0Parametrized1DValue =0._dl
          return
        end if

        if (self%b ==0._dl .and. self%c ==0._dl .and. self%alpha ==0._dl) then
          H0Parametrized1DValue = -1._dl
        else
          H0Parametrized1DValue =  -(a**(2 + c)*(3 + c)*h0**2 - 3*(-1 + exp(a**b*al))*gpim_tot + a**b*b*exp(a**b*al)*al*grhom_tot)/(3.*(a**(2 + c)*h0**2 &
              &+ (-1 + exp(a**b*al))*grhom_tot))

        end if

    end function H0Parametrized1DValue

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the value of the first derivative, wrt scale factor, of the function.
    function H0Parametrized1DFirstDerivative( self, x, eft_cache, eft_par_cache )

        implicit none

        class(H0_parametrization_1D)                        :: self      !< the base class
        real(dl), intent(in)                                :: x         !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(in), optional :: eft_par_cache !< the optional input EFTCAMB cache
        type(EFTCAMB_timestep_cache), intent(in), optional  :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: H0Parametrized1DFirstDerivative                     !< the output value

        real(dl) :: ab,ac,al,grhom_tot,gpim_tot,h0,c,b, a
        real(dl) :: gpim_dot

        a = x
        b  = self%b
        c  = self%c
        al = self%alpha
        h0  = eft_par_cache%h0_Mpc
        ! gpim_dot = 1._dl/(x**3*eft_cache%adotoa)*eft_cache%gpresdotm_t
        ! grhom_prime = -3._dl/(x**3)*(eft_cache%gpresm_t +eft_cache%grhom_t )

        grhom_tot = eft_cache%grhob_t+eft_cache%grhoc_t+eft_cache%grhor_t+eft_cache%grhog_t +eft_cache%grhonu_tot
        gpim_tot  = 1._dl/3._dl*(eft_cache%grhor_t+eft_cache%grhog_t) +eft_cache%gpinu_tot

        !missing massive nu!!
        gpim_dot = -4._dl*h0**2/a**2*eft_cache%adotoa*( eft_par_cache%omegag + eft_par_cache%omegar )
        if (x<1.E-5_dl) then
          H0Parametrized1DFirstDerivative =0._dl
          return
        end if

        if (self%b*self%c*self%alpha ==0._dl) then
          return
        else
        H0Parametrized1DFirstDerivative = (3*(-1 + exp(a**b*al))*gpim_dot*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot) &
              &+eft_cache%adotoa*(-(a**(2*b)*b**2*exp(a**b*al)*al**2*(a**(2 + c)*h0**2 - grhom_tot)*grhom_tot) +(-1 + exp(a**b*al))*(9*(-1 &
              &+ exp(a**b*al))*gpim_tot*(gpim_tot + grhom_tot) -a**(2 + c)*h0**2*((9 + 6*c)*gpim_tot + (3 + c)**2*grhom_tot)) &
              &+a**b*b*exp(a**b*al)*al*(-(b*(-1 + exp(a**b*al))*grhom_tot**2) +a**(2 + c)*h0**2*(6*gpim_tot + (6 - b &
              &+ 2*c)*grhom_tot))))/(3.*a*eft_cache%adotoa*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**2)

        end if

    end function H0Parametrized1DFirstDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the second derivative of the function.
    function H0Parametrized1DSecondDerivative( self, x, eft_cache, eft_par_cache )

        implicit none

        class(H0_parametrization_1D)                        :: self      !< the base class
        real(dl), intent(in)                                :: x         !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(in), optional :: eft_par_cache !< the optional input EFTCAMB cache
        type(EFTCAMB_timestep_cache), intent(in), optional  :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: H0Parametrized1DSecondDerivative                !< the output value

        real(dl) :: ab,ac,al,grhom_tot,gpim_tot,h0, c,b, a
        real(dl) :: gpim_dot, gpim_dotdot

        a = x
        b  = self%b
        c  = self%c
        al = self%alpha
        h0  = eft_par_cache%h0_Mpc
        ! gpim_dot = 1._dl/(x**3*eft_cache%adotoa)*eft_cache%gpresdotm_t
        ! grhom_prime = -3._dl/(x**3)*(eft_cache%gpresm_t +eft_cache%grhom_t )

        if (x<1.E-5_dl) then
          H0Parametrized1DSecondDerivative =0._dl
          return
        end if

        grhom_tot = eft_cache%grhob_t+eft_cache%grhoc_t+eft_cache%grhor_t+eft_cache%grhog_t +eft_cache%grhonu_tot
        gpim_tot  = 1._dl/3._dl*(eft_cache%grhor_t+eft_cache%grhog_t) +eft_cache%gpinu_tot

        !missing massive nu!!
        gpim_dot = -4._dl*h0**2/a**2*eft_cache%adotoa*( eft_par_cache%omegag + eft_par_cache%omegar )
        gpim_dotdot = -4._dl*h0**2/a**2*(eft_cache%Hdot -4._dl*eft_cache%adotoa**2)*( eft_par_cache%omegag + eft_par_cache%omegar )

        if (self%b*self%c*self%alpha ==0._dl) then
          return
        else

        H0Parametrized1DSecondDerivative = -(2*eft_cache%adotoa**3*(a**(2 + c)*(3 + c)*h0**2 - 3*(-1 + exp(a**b*al))*gpim_tot &
            &+a**b*b*exp(a**b*al)*al*grhom_tot)*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot -3*(-1 + exp(a**b*al))*(gpim_tot &
            &+ grhom_tot))**2 +eft_cache%adotoa**2*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)*(a**(2 + c)*(3 + c)*h0**2 - 3*(-1 &
            &+ exp(a**b*al))*gpim_tot + a**b*b*exp(a**b*al)*al*grhom_tot)*(3*(-1 + exp(a**b*al))*gpim_dot + eft_cache%adotoa*(-(a**(2 + c)*(-1 &
            &+ c)*c*h0**2) +6*(2 + exp(a**b*al)*(-2 + a**b*b*al))*gpim_tot -(-12 + exp(a**b*al)*(12 + a**b*b*al*(-7 + b + a**b*b*al)))*grhom_tot)) &
            &-2*eft_cache%adotoa**2*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot &
            &-3*(-1 + exp(a**b*al))*(gpim_tot + grhom_tot))*(-3*(-1 + exp(a**b*al))*gpim_dot + eft_cache%adotoa*(a**(2 + c)*c*(3 + c)*h0**2 &
            &+a**b*b*exp(a**b*al)*al*(-6*gpim_tot + (-3 + b + a**b*b*al)*grhom_tot))) +(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**2*(&
            &-3*(1 + exp(a**b*al)*(-1 + 3*a**b*b*al))*eft_cache%adotoa**2*gpim_dot + 3*(-1 + exp(a**b*al))*eft_cache%Hdot*gpim_dot -3*(-1 &
            &+ exp(a**b*al))*eft_cache%adotoa*gpim_dotdot + eft_cache%adotoa**3*(a**(2 + c)*(-1 + c)*c*(3 + c)*h0**2 +a**b*b*exp(a**b*al)*al*(-3*(-5 &
            &+ 3*b + 3*a**b*b*al)*gpim_tot +(12 + b*(-7 + b + a**b*(-7 + 3*b)*al + a**(2*b)*b*al**2))*grhom_tot))))/&
            &(3.*a**2*eft_cache%adotoa**3*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**3)

        end if

    end function H0Parametrized1DSecondDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the third derivative of the function.
    function H0Parametrized1DThirdDerivative( self, x, eft_cache, eft_par_cache )

        implicit none

        class(H0_parametrization_1D)                        :: self      !< the base class
        real(dl), intent(in)                                :: x         !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(in), optional :: eft_par_cache !< the optional input EFTCAMB cache
        type(EFTCAMB_timestep_cache), intent(in), optional  :: eft_cache !< the optional input EFTCAMB cache
        real(dl) :: H0Parametrized1DThirdDerivative                 !< the output value

        real(dl) :: ab,ac,al,grhom_tot,gpim_tot,h0,c,b,a
        real(dl) :: gpim_dot, gpim_dotdot, gpim_dotdotdot

        a = x
        b  = self%b
        c  = self%c
        al = self%alpha
        h0  = eft_par_cache%h0_Mpc
        ! gpim_dot = 1._dl/(x**3*eft_cache%adotoa)*eft_cache%gpresdotm_t
        ! grhom_prime = -3._dl/(x**3)*(eft_cache%gpresm_t +eft_cache%grhom_t )
        if (x<1.E-5_dl) then
          H0Parametrized1DThirdDerivative =0._dl
          return
        end if

        grhom_tot = eft_cache%grhob_t+eft_cache%grhoc_t+eft_cache%grhor_t+eft_cache%grhog_t +eft_cache%grhonu_tot
        gpim_tot  = 1._dl/3._dl*(eft_cache%grhor_t+eft_cache%grhog_t) +eft_cache%gpinu_tot

        !missing massive nu!!
        gpim_dot = -4._dl*h0**2/a**2*eft_cache%adotoa*( eft_par_cache%omegag + eft_par_cache%omegar )
        gpim_dotdot = -4._dl*h0**2/a**2*(eft_cache%Hdot -4._dl*eft_cache%adotoa**2)*( eft_par_cache%omegag + eft_par_cache%omegar )
        gpim_dotdotdot = -4._dl*h0**2/a**2*(eft_cache%Hdotdot -12._dl*eft_cache%adotoa*eft_cache%Hdot +16._dl*eft_cache%adotoa**3)*( eft_par_cache%omegag + eft_par_cache%omegar )

        if (self%b*self%c*self%alpha ==0._dl) then
          return
        else

        H0Parametrized1DThirdDerivative = (6*eft_cache%adotoa**5*(a**(2 + c)*(3 + c)*h0**2 - 3*(-1 + exp(a**b*al))*gpim_tot &
            &+a**b*b*exp(a**b*al)*al*grhom_tot)*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot -3*(-1 + exp(a**b*al))*(gpim_tot &
            &+ grhom_tot))**3 +6*eft_cache%adotoa**4*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)*(a**(2 + c)*(3 + c)*h0**2 &
            &- 3*(-1 + exp(a**b*al))*gpim_tot + a**b*b*exp(a**b*al)*al*grhom_tot)*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot &
            &-3*(-1 + exp(a**b*al))*(gpim_tot + grhom_tot))*(3*(-1 + exp(a**b*al))*gpim_dot + eft_cache%adotoa*(-(a**(2 + c)*(-1 + c)*c*h0**2) &
            &+6*(2 + exp(a**b*al)*(-2 + a**b*b*al))*gpim_tot -(-12 + exp(a**b*al)*(12 + a**b*b*al*(-7 + b + a**b*b*al)))*grhom_tot)) &
            &+eft_cache%adotoa**2*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**2*(a**(2 + c)*(3 + c)*h0**2 - 3*(-1 &
            &+ exp(a**b*al))*gpim_tot + a**b*b*exp(a**b*al)*al*grhom_tot)*(-9*(2 + exp(a**b*al)*(-2 + a**b*b*al))*eft_cache%adotoa**2*gpim_dot &
            &+ 3*(-1 + exp(a**b*al))*eft_cache%Hdot*gpim_dot -3*(-1 + exp(a**b*al))*eft_cache%adotoa*gpim_dotdot + eft_cache%adotoa**3*(a**(2 &
            &+ c)*(-2 + c)*(-1 + c)*c*h0**2 +3*(20 - exp(a**b*al)*(20 + 3*a**b*b*al*(-5 + b + a**b*b*al)))*gpim_tot +(60 &
            &+ exp(a**b*al)*(-60 + a**b*b*al*(47 + b*(-12 + b + 3*a**b*(-4 + b)*al + a**(2*b)*b*al**2))))*grhom_tot)) &
            &+3*eft_cache%adotoa**3*(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**2*(3*(-1 + exp(a**b*al))*gpim_dot &
            &+ eft_cache%adotoa*(-(a**(2 + c)*(-1 + c)*c*h0**2) +6*(2 + exp(a**b*al)*(-2 + a**b*b*al))*gpim_tot -(-12&
            &+ exp(a**b*al)*(12 + a**b*b*al*(-7 + b + a**b*b*al)))*grhom_tot))*(3*(-1 + exp(a**b*al))*gpim_dot - eft_cache%adotoa*(a**(2 &
            &+ c)*c*(3 + c)*h0**2 +a**b*b*exp(a**b*al)*al*(-6*gpim_tot + (-3 + b + a**b*b*al)*grhom_tot))) -6*eft_cache%adotoa**4*(a**(2 &
            &+ c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot -3*(-1 + exp(a**b*al))*(gpim_tot &
            &+ grhom_tot))**2*(-3*(-1 + exp(a**b*al))*gpim_dot + eft_cache%adotoa*(a**(2 + c)*c*(3 + c)*h0**2 +a**b*b*exp(a**b*al)*al*(&
            &-6*gpim_tot + (-3 + b + a**b*b*al)*grhom_tot))) +3*eft_cache%adotoa**2*(a**(2 + c)*h0**2 + (-1 &
            &+ exp(a**b*al))*grhom_tot)**2*(a**(2 + c)*c*h0**2 + a**b*b*exp(a**b*al)*al*grhom_tot -3*(-1 + exp(a**b*al))*(gpim_tot &
            &+ grhom_tot))*(-3*(1 + exp(a**b*al)*(-1 + 3*a**b*b*al))*eft_cache%adotoa**2*gpim_dot + 3*(-1 &
            &+ exp(a**b*al))*eft_cache%Hdot*gpim_dot -3*(-1 + exp(a**b*al))*eft_cache%adotoa*gpim_dotdot + eft_cache%adotoa**3*(a**(2 &
            &+ c)*(-1 + c)*c*(3 + c)*h0**2 +a**b*b*exp(a**b*al)*al*(-3*(-5 + 3*b + 3*a**b*b*al)*gpim_tot +(12 + b*(-7 + b + a**b*(-7 &
            &+ 3*b)*al + a**(2*b)*b*al**2))*grhom_tot))) -(a**(2 + c)*h0**2 + (-1 + exp(a**b*al))*grhom_tot)**3*(-6*(-1 + exp(a**b*al)*(1 &
            &+ 3*a**b*b*al*(-2 + b + a**b*b*al)))*eft_cache%adotoa**4*gpim_dot - 9*(-1 + exp(a**b*al))*eft_cache%Hdot**2*gpim_dot -3*(3 &
            &+ exp(a**b*al)*(-3 + 4*a**b*b*al))*eft_cache%adotoa**3*gpim_dotdot +3*(-1 + exp(a**b*al))*eft_cache%adotoa*(eft_cache%Hdotdot*gpim_dot &
            &+ 3*eft_cache%Hdot*gpim_dotdot) +3*eft_cache%adotoa**2*((3 + exp(a**b*al)*(-3 + 4*a**b*b*al))*eft_cache%Hdot*gpim_dot + gpim_dotdotdot &
            &- exp(a**b*al)*gpim_dotdotdot) + eft_cache%adotoa**5*(a**(2 + c)*c*(6 - 7*c + c**3)*h0**2 +a**b*b*exp(a**b*al)*al*(-6*(11 &
            &+ b*(-9*(1 + a**b*al) + 2*b*(1 + a**b*al*(3 + a**b*al))))*gpim_tot +(-60 + b*(47*(1 + a**b*al) + b*(-12 + b + a**b*(-36 &
            &+ 7*b)*al + 6*a**(2*b)*(-2 + b)*al**2 + a**(3*b)*b*al**3)))*grhom_tot))))/(3.*a**3*eft_cache%adotoa**5*(a**(2 + c)*h0**2 &
            &+ (-1 + exp(a**b*al))*grhom_tot)**4)

        end if
    end function H0Parametrized1DThirdDerivative

    ! ---------------------------------------------------------------------------------------------
    !> Function that returns the integral of the function, as defined in the notes.
    function H0Parametrized1DIntegral( self, x, eft_cache, eft_par_cache )

        implicit none

        class(H0_parametrization_1D)                        :: self      !< the base class
        real(dl), intent(in)                                :: x         !< the input scale factor
        type(EFTCAMB_timestep_cache), intent(in), optional  :: eft_cache !< the optional input EFTCAMB cache
        type(EFTCAMB_parameter_cache), intent(in), optional :: eft_par_cache !< the optional input EFTCAMB cache
        real(dl) :: H0Parametrized1DIntegral                        !< the output value

        real(dl) :: ab,ac,al,grhom_tot,gpim_tot,h0,c,b,a

        a = x
        b  = self%b
        c  = self%c
        al = self%alpha
        h0  = eft_par_cache%h0_Mpc
        if (self%b*self%c*self%alpha ==0._dl) then
          H0Parametrized1DIntegral =x**2
        else
          H0Parametrized1DIntegral = ( eft_cache%grhom_t/h0**2*( exp( al*a**b ) -1._dl )+a**( c +2 ) )/(3._dl*eft_par_cache%omegav)
        end if

    end function H0Parametrized1DIntegral

    ! ---------------------------------------------------------------------------------------------

end module EFTCAMB_H0_parametrizations_1D

!----------------------------------------------------------------------------------------
