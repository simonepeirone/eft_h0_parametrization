!----------------------------------------------------------------------------------------
!
! This file is part of EFTCAMB.
!
! Copyright (C) 2013-2017 by the EFTCAMB authors
!
! The EFTCAMB code is free software;
! You can use it, redistribute it, and/or modify it under the terms
! of the GNU General Public License as published by the Free Software Foundation;
! either version 3 of the License, or (at your option) any later version.
! The full text of the license can be found in the file eftcamb/LICENSE at
! the top level of the EFTCAMB distribution.
!
!----------------------------------------------------------------------------------------

!> @file 02_utilities.f90
!! This file contains various generic algorithms that are useful to EFTCAMB.


!----------------------------------------------------------------------------------------
!> This module contains various generic algorithms that are useful to EFTCAMB.

!> @author Bin Hu, Marco Raveri, Simone Peirone

module EFTCAMB_mixed_algorithms

    use precision

    implicit none

    private

    public hunt, integer_to_string, string, gammp, gammln, gammq

    !----------------------------------------------------------------------------------------
    !> This is a utility type that allows to handle strings arrays. It is used because
    !! of partial coverage of these F2003 features that would prevent compiling with gfortran
    type :: string
        character(len=:), allocatable :: string
    end type string

contains

    ! ---------------------------------------------------------------------------------------------
    !> Hunting algorithm: This is used to efficiently search an ordered table by means of a
    !> hunting and a bisection algorithm.
    subroutine hunt(xx,n,x,jlo)

        implicit none

        integer  :: n      !< the length of the table
        real(dl) :: xx(n)  !< the table to be searched
        real(dl) :: x      !< the requested value
        integer  :: jlo    !< the index of the closest (from below) entry of the table

        integer inc,jhi,jm
        logical ascnd
        ascnd=xx(n).ge.xx(1)
        if(jlo.le.0.or.jlo.gt.n)then
            jlo=0
            jhi=n+1
            goto 3
        endif
        inc=1
        if(x.ge.xx(jlo).eqv.ascnd)then
1           jhi=jlo+inc
            if(jhi.gt.n)then
                jhi=n+1
            else if(x.ge.xx(jhi).eqv.ascnd)then
                jlo=jhi
                inc=inc+inc
                goto 1
            endif
        else
            jhi=jlo
2           jlo=jhi-inc
            if(jlo.lt.1)then
                jlo=0
            else if(x.lt.xx(jlo).eqv.ascnd)then
                jhi=jlo
                inc=inc+inc
                goto 2
            endif
        endif
3       if(jhi-jlo.eq.1)then
            if(x.eq.xx(n))jlo=n-1
            if(x.eq.xx(1))jlo=1
            return
        endif
        jm=(jhi+jlo)/2
        if(x.ge.xx(jm).eqv.ascnd)then
            jlo=jm
        else
            jhi=jm
        endif
        goto 3

    end subroutine hunt

    ! ---------------------------------------------------------------------------------------------
    !> This function converts an integer to a string. Usefull for numbered files output.
    function integer_to_string( number )

        implicit none

        integer, intent(in) :: number               !< Input integer number
        character(10)       :: integer_to_string    !< Output string with the number

        write( integer_to_string, '(i10)' ) number

        integer_to_string = TRIM(ADJUSTL( integer_to_string ))

    end function integer_to_string

    !----------------------------------------------------------------------------------------

    function gammln(xx)
      real(dl),intent(in) :: xx
      real(dl) :: gammln
      real(dl) :: ser,stp,tmp,x,y,cof(6)
      integer  :: j
      SAVE cof,stp
      DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,-.5395239384953d-5,2.5066282746310005d0/

      x=xx
      y=x
      tmp=x+5.5_dl
      tmp=(x+0.5_dl)*log(tmp)-tmp
      ser=1.000000000190015d0
      do j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
      enddo
      gammln=tmp+log(stp*ser/x)
      return
    end function gammln

    subroutine gser(gamser,a,x,gln)
      integer  ::  n
      real(dl), intent(out) :: gamser
      real(dl), intent(in) :: a,x
      real(dl) :: gln
      real(dl), parameter :: EPS=3.e-7
      integer, parameter :: ITMAX=100
      real(dl) :: ap,del,sum!,gammln

      gln=gammln(a)
      if(x.le.0._dl)then
        if(x.lt.0._dl) return
      end if
      ap=a
      sum=1._dl/a
      del=sum

      do  n=1,ITMAX
        ap=ap+1._dl
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*EPS)goto 12
      enddo
      12 gamser=sum*exp(-x+a*log(x)-gln)
      return
    end subroutine gser

    subroutine gcf(gammcf,a,x,gln)
      integer, parameter :: ITMAX=100
      real(dl), intent(in) :: a, x
      real(dl), intent(out) :: gammcf
      real(dl) :: gln, an,b,c,d,del,h!,gammln
      real(dl), parameter :: EPS=3.e-7,FPMIN=1.e-30
      integer ::i

      gln=gammln(a)
      b=x+1._dl-a
      c=1._dl/FPMIN
      d=1._dl/b
      h=d

      do i=1,ITMAX
        an=-i*(i-a)
        b=b+2._dl
        d=an*d+b

        if(abs(d).lt.FPMIN) d=FPMIN
        c=b+an/c
        if(abs(c).lt.FPMIN) c=FPMIN
        d=1._dl/d
        del=d*c
        h=h*del
        if(abs(del-1.).lt.EPS)  goto 13
      enddo
      13 gammcf=exp(-x+a*log(x)-gln)*h
      return
    end subroutine gcf

    function gammp(a,x)
      real(dl), intent(in) :: a,x
      real(dl) :: gammp
      real(dl) :: gammcf,gamser,gln

      if(x .lt. 0._dl .or. a .le. 0._dl) print*, "bad arguments in gammp"
      if(x .lt. a+1._dl)then
        call gser(gamser,a,x,gln)
        gammp=gamser
      else
        call gcf(gammcf,a,x,gln)
        gammp=1._dl-gammcf
      endif
      return
    end function gammp

    function gammq(a,x)
      real(dl), intent (in) :: a,x
      real(dl)  :: gammq
      real(dl)  :: gammcf,gamser,gln
    if(x.lt.0..or.a.le.0.)pause 'bad arguments in gammq'
    if(x.lt.a+1.)then
      call gser(gamser,a,x,gln)
      gammq=1.-gamser
    else
      call gcf(gammcf,a,x,gln)
      gammq=gammcf
    endif
    return

  end function gammq

  end module EFTCAMB_mixed_algorithms

!----------------------------------------------------------------------------------------
