!----------------------------------------------------------------------------------------
!
! This file is part of EFTCAMB.
!
! Copyright (C) 2013-2017 by the EFTCAMB authors
!
! The EFTCAMB code is free software;
! You can use it, redistribute it, and/or modify it under the terms
! of the GNU General Public License as published by the Free Software Foundation;
! either version 3 of the License, or (at your option) any later version.
! The full text of the license can be found in the file eftcamb/LICENSE at
! the top level of the EFTCAMB distribution.
!
!----------------------------------------------------------------------------------------

!> @file 09p3_Designer_H0.f90
!! This file contains the relevant code for designer H0 model.


!----------------------------------------------------------------------------------------
!> This module contains the relevant code for H0 model.

!> @author Simone Peirone

module EFTCAMB_designer_H0

    use precision
    use IniFile
    use AMLutils
    use equispaced_linear_interpolation_1D
    use EFT_def
    use EFTCAMB_rootfind
    use EFTCAMB_cache
    use EFTCAMB_abstract_parametrizations_1D
    use EFTCAMB_neutral_parametrization_1D
    use EFTCAMB_constant_parametrization_1D
    use EFTCAMB_CPL_parametrizations_1D
    use EFTCAMB_JBP_parametrizations_1D
    use EFTCAMB_turning_point_parametrizations_1D
    use EFTCAMB_taylor_parametrizations_1D
    use EFTCAMB_H0_parametrizations_1D
    use EFTCAMB_abstract_model_designer
    use EFTCAMB_mixed_algorithms

    implicit none

    private

    public EFTCAMB_des_H0

    !----------------------------------------------------------------------------------------
    !> This is the designer H0 model. Inherits from the abstract designer model and has the
    !! freedom of defining the expansion history.
    type, extends ( EFTCAMB_designer_model ) :: EFTCAMB_des_H0

        ! theory parameters:
        real(dl) :: alpha
        real(dl) :: b
        real(dl) :: c
        !real(dl) :: h0

        ! the pure EFT functions:
        class( parametrized_function_1D ), allocatable :: DesH0wDE      !< The pure EFT function w_DE.

        ! the pure EFT functions model selection flags:
        integer  :: EFTwDE                                              !< Model selection flag for designer f(R) w DE.

        ! the interpolated EFT functions that come out of the background sover:
        type(equispaced_linear_interpolate_function_1D) :: EFTOmega       !< The interpolated function Omega (and derivatives).
        ! type(equispaced_linear_interpolate_function_1D) :: EFTLambda      !< The interpolated function Lambda (and derivatives).

        ! some designer parameters:
        integer  :: designer_num_points = 100000                     !< Number of points sampled by the designer code.
        real(dl) :: x_initial           = log(10._dl**(-8._dl))           !< log(a start)
        real(dl) :: x_final             = 0.0_dl                          !< log(a final)

    contains

        ! initialization of the model:
        procedure :: read_model_selection            => EFTCAMBDesH0ReadModelSelectionFromFile   !< subroutine that reads the parameters of the model from file
        procedure :: allocate_model_selection        => EFTCAMBDesH0AllocateModelSelection       !< subroutine that allocates the model selection.
        procedure :: init_model_parameters           => EFTCAMBDesH0InitModelParameters          !< subroutine taht initializes the model parameters based on the values found in an input array.
        procedure :: init_model_parameters_from_file => EFTCAMBDesH0InitModelParametersFromFile  !< subroutine that reads the parameters of the model from file.

        ! background solver:
        procedure :: initialize_background           => EFTCAMBDesignerH0InitBackground               !< subroutine that initializes the background of designer f(R).
        procedure :: solve_designer_equations        => EFTCAMBDesignerH0SolveDesignerEquations       !< subroutine that solves the designer f(R) background equations.

        ! utility functions:
        procedure :: compute_param_number  => EFTCAMBDesH0ComputeParametersNumber     !< subroutine that computes the number of parameters of the model.
        procedure :: feedback              => EFTCAMBDesH0Feedback                    !< subroutine that prints on the screen feedback information about the model.
        procedure :: parameter_names       => EFTCAMBDesH0ParameterNames              !< subroutine that returns the i-th parameter name of the model.
        procedure :: parameter_names_latex => EFTCAMBDesH0ParameterNamesLatex         !< subroutine that returns the i-th parameter name of the model.
        procedure :: parameter_values      => EFTCAMBDesH0ParameterValues             !< subroutine that returns the i-th parameter value.

        ! CAMB related procedures:
        procedure :: compute_background_EFT_functions  => EFTCAMBDesH0BackgroundEFTFunctions   !< subroutine that computes the value of the background EFT functions at a given time.
        procedure :: compute_secondorder_EFT_functions => EFTCAMBDesH0SecondOrderEFTFunctions  !< subroutine that computes the value of the second order EFT functions at a given time.
        procedure :: compute_dtauda                    => EFTCAMBDesH0ComputeDtauda            !< function that computes dtauda = 1/sqrt(a^2H^2).
        procedure :: compute_adotoa                    => EFTCAMBDesH0ComputeAdotoa            !< subroutine that computes adotoa = H and its two derivatives wrt conformal time.
        procedure :: compute_H_derivs                  => EFTCAMBDesH0ComputeHubbleDer         !< subroutine that computes the two derivatives wrt conformal time of H.

        ! stability procedures:
        procedure :: additional_model_stability        => EFTCAMBDesH0AdditionalModelStability !< function that computes model specific stability requirements.

    end type EFTCAMB_des_H0

    ! ---------------------------------------------------------------------------------------------

contains

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that reads the parameters of the model from file.
    subroutine EFTCAMBDesH0ReadModelSelectionFromFile( self, Ini )

        implicit none

        class(EFTCAMB_des_H0)       :: self   !< the base class
        type(TIniFile)              :: Ini    !< Input ini file

        ! read model selection flags:
        self%EFTwDE             = Ini_Read_Int_File( Ini, 'EFTwDE', 6 )

    end subroutine EFTCAMBDesH0ReadModelSelectionFromFile

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that allocates the model selection.
    subroutine EFTCAMBDesH0AllocateModelSelection( self )

        implicit none

        class(EFTCAMB_des_H0)                             :: self              !< the base class
        character, allocatable, dimension(:)              :: param_names       !< an array of strings containing the names of the function parameters
        character, allocatable, dimension(:)              :: param_names_latex !< an array of strings containing the latex names of the function parameters

        ! allocate wDE:
        if ( allocated(self%DesH0wDE) ) deallocate(self%DesH0wDE)
        select case ( self%EFTwDE )
            case(0)
                allocate( wDE_LCDM_parametrization_1D::self%DesH0wDE )
            case(1)
                allocate( constant_parametrization_1D::self%DesH0wDE )
            case(2)
                allocate( CPL_parametrization_1D::self%DesH0wDE )
                call self%DesH0wDE%set_param_names( ['EFTw0', 'EFTwa'], ['w_0', 'w_a'] )
            case(3)
                allocate( JBP_parametrization_1D::self%DesH0wDE )
                call self%DesH0wDE%set_param_names( ['EFTw0', 'EFTwa', 'EFTwn'], [ 'w_0', 'w_a', 'n  ' ] )
            case(4)
                allocate( turning_point_parametrization_1D::self%DesH0wDE )
                call self%DesH0wDE%set_param_names( ['EFTw0 ', 'EFTwa ', 'EFTwat'], ['w_0', 'w_a', 'a_t'] )
            case(5)
                allocate( taylor_parametrization_1D::self%DesH0wDE )
                call self%DesH0wDE%set_param_names( ['EFTw0', 'EFTwa', 'EFTw2', 'EFTw3'], ['w_0', 'w_a', 'w_2', 'w_3'] )
            case(6)
                allocate( H0_parametrization_1D::self%DesH0wDE )
                call self%DesH0wDE%set_param_names( ['H0_alpha', 'H0_b', 'H0_c'], ['\alpha', 'b', 'c'] )
            case default
                write(*,'(a,I3)') 'No model corresponding to EFTwDE =', self%EFTwDE
                write(*,'(a)')    'Please select an appropriate model.'
        end select

        ! initialize the names:
        call self%DesH0wDE%set_name( 'EFTw', 'w' )

    end subroutine EFTCAMBDesH0AllocateModelSelection

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that initializes the model parameters based on the values found in an input array.
    subroutine EFTCAMBDesH0InitModelParameters( self, array )

        implicit none

        class(EFTCAMB_des_H0)                                  :: self   !< the base class
        real(dl), dimension(self%parameter_number), intent(in) :: array  !< input array with the values of the parameters.
        integer                                                :: i

        self%b     = array(1)
        self%c     = array(2)

        ! call self%DesH0wDE%init_parameters(array)

    end subroutine EFTCAMBDesH0InitModelParameters

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that reads the parameters of the model from file.
    subroutine EFTCAMBDesH0InitModelParametersFromFile( self, Ini )

        implicit none

        class(EFTCAMB_des_H0)       :: self   !< the base class
        type(TIniFile)              :: Ini    !< Input ini file


        ! read parameters:
        ! self%alpha = Ini_Read_Double_File( Ini, 'H0_alpha', 0._dl )
        self%b     = Ini_Read_Double_File( Ini, 'H0_b', 0._dl )
        self%c     = Ini_Read_Double_File( Ini, 'H0_c', 0._dl )
        ! self%h0    = Ini_Read_Double_File( Ini, 'hubble', 0._dl )

        ! read w_DE parameters:
        ! call self%DesH0wDE%init_from_file( Ini )

    end subroutine EFTCAMBDesH0InitModelParametersFromFile

    !> Subroutine that initializes the background of designer f(R).
    subroutine EFTCAMBDesignerH0InitBackground( self, params_cache, feedback_level, success )

        implicit none

        class(EFTCAMB_des_H0)                   :: self           !< the base class
        type(EFTCAMB_parameter_cache), intent(in)    :: params_cache   !< a EFTCAMB parameter cache containing cosmological parameters
        integer                      , intent(in)    :: feedback_level !< level of feedback from the background code. 0=none; 1=some; 2=chatty.
        logical                      , intent(out)   :: success        !< wether the background initialization succeded or not
        real(dl), dimension(3)                       :: array  !< input array with the values of the parameters.

        real(dl) :: A_ini, Omega0_tot
        real(dl) :: TempMin, TempMax, debug_A
        integer  :: Debug_MaxNum, Debug_n

        ! compute alpha from flatness constraint
        Omega0_tot = params_cache%omegac+ params_cache%omegab+ params_cache%omegan + params_cache%omegag + params_cache%omegar
        self%alpha = log(2._dl/(3._dl*Omega0_tot))

        !initialize wDE
        array(1) = self%alpha
        array(2) = self%b
        array(3) = self%c
        call self%DesH0wDE%init_parameters(array)
        call self%DesH0wDE%feedback()


        ! ! some feedback:
        ! if ( feedback_level>0 ) then
        !     write(*,'(a)') "***************************************************************"
        !     write(*,'(a)') ' EFTCAMB designer f(R) background solver'
        !     write(*,'(a)')
        ! end if

        ! initialize interpolating functions:
        call self%EFTOmega%initialize  ( self%designer_num_points, self%x_initial, self%x_final )
        ! call self%EFTLambda%initialize ( self%designer_num_points, self%x_initial, self%x_final )

        ! debug code:
        ! if ( DebugEFTCAMB ) then
        !     ! print the function B0(A). This is used to debug the initial conditions part.
        !     call CreateTxtFile( './debug_designer_fR_B.dat', 34 )
        !     print*, 'EFTCAMB DEBUG ( f(R) designer ): Printing B(A) results'
        !     TempMin      = -10._dl
        !     TempMax      = +10._dl
        !     Debug_MaxNum = 1000
        !     do Debug_n = 1, Debug_MaxNum
        !         debug_A = TempMin +REAL(Debug_n-1)*(TempMax-TempMin)/REAL(Debug_MaxNum-1)
        !         call self%solve_designer_equations( params_cache, debug_A, B0, only_B0=.True., success=success )
        !         write(34,*) debug_A, B0
        !     end do
        !     close(34)
        !     ! prints f(R) quantities.
        !     print*, 'EFTCAMB DEBUG ( f(R) designer ): Printing F(R) results'
        !     call CreateTxtFile( './debug_designer_fR_solution.dat', 33 )
        !     debug_A = 1.0_dl
        !     call self%solve_designer_equations( params_cache, debug_A, B0, only_B0=.False., success=success )
        !     close(33)
        ! end if

        ! call boundary conditions lookup:
        ! call self%find_initial_conditions( params_cache, feedback_level, A_ini, success )

        ! solve the background equations and store the solution:
        ! call self%solve_designer_equations( params_cache, A_ini, success=success )
        success =.true.

    end subroutine EFTCAMBDesignerH0InitBackground

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that solves the designer H0 background equations.
    subroutine EFTCAMBDesignerH0SolveDesignerEquations( self, params_cache, A, success )

        implicit none

        class(EFTCAMB_des_H0)                   :: self          !< the base class.
        type(EFTCAMB_parameter_cache), intent(in)    :: params_cache  !< a EFTCAMB parameter cache containing cosmological parameters.
        real(dl), intent(in)                         :: A             !< the initial value of the A coefficient. Refer to the numerical notes for the details.
        logical , intent(out)                        :: success       !< whether the calculation ended correctly or not

        integer, parameter :: num_eq = 1   !<  Number of equations

        real(dl) :: Omegam_EFT, Omegavac_EFT, OmegaMassiveNu_EFT, OmegaGamma_EFT, OmegaNu_EFT
        real(dl) :: Omegarad_EFT, EquivalenceScale_fR, Ratio_fR, Initial_B_fR, Initial_C_fR
        real(dl) :: PPlus, yPlus, CoeffA_Part, yStar, x

        real(dl) :: y(num_eq), ydot(num_eq)

        integer  :: itol, itask, istate, iopt, LRN, LRS, LRW, LIS, LIN, LIW, JacobianMode, i
        real(dl) :: rtol, atol, t1, t2, B
        real(dl), allocatable :: rwork(:)
        integer,  allocatable :: iwork(:)

        ! Set initial conditions:
        x    = self%x_initial
        y(1) = 0._dl
        ydot(1) = 0._dl

        ! Initialize DLSODA:
        ! set-up the relative and absolute tollerances:
        itol = 1
        rtol = 1.d-13
        atol = 1.d-14
        ! initialize task to do:
        itask  = 1
        istate = 1
        iopt   = 1
        ! initialize the work space:
        LRN = 20 + 16*num_eq
        LRS = 22 + 9*num_eq + num_eq**2
        LRW = max(LRN,LRS)
        LIS = 20 + num_eq
        LIN = 20
        LIW = max(LIS,LIN)
        ! allocate the arrays:
        allocate(rwork(LRW))
        allocate(iwork(LIW))
        ! optional lsoda input:
        RWORK(5) = 0._dl  ! the step size to be attempted on the first step. The default value is determined by the solver.
        RWORK(6) = 0.00001_dl  ! the maximum absolute step size allowed. The default value is infinite.
        RWORK(7) = 0._dl  ! the minimum absolute step size allowed. The default value is 0.
        IWORK(5) = 0      ! flag to generate extra printing at method switches. IXPR = 0 means no extra printing (the default). IXPR = 1 means print data on each switch.
        IWORK(6) = 500    ! maximum number of (internally defined) steps allowed during one call to the solver. The default value is 500.
        IWORK(7) = 0      ! maximum number of messages printed (per problem) warning that T + H = T on a step (H = step size). This must be positive to result in a non-default value.  The default value is 10.
        IWORK(8) = 12      ! the maximum order to be allowed for the nonstiff (Adams) method.  the default value is 12.
        IWORK(9) = 5      ! the maximum order to be allowed for the stiff (BDF) method.  The default value is 5.
        ! additional lsoda stuff:
        CALL XSETF(0) ! suppress odepack printing
        ! Jacobian mode: 1=fullJacobian, 2=not provided
        JacobianMode = 2

        ! store the first value of the EFT functions:
        t1  = self%EFTOmega%x(1)

        ! compute output EFT functions if needed:
        call output( num_eq, 1, t1, y )

        ! solve the equations:
        do i=1, self%EFTOmega%num_points-1

            ! set the time step:
            t1 = self%EFTOmega%x(i)
            t2 = self%EFTOmega%x(i+1)
            ! solve the system:
            call DLSODA ( derivs, num_eq, y, t1, t2, itol, rtol, atol, itask, istate, iopt, RWORK, LRW, IWORK, LIW, jacobian, JacobianMode)
            ! check istate for LSODA good completion:
            if ( istate < 0 ) then
                success = .False.
                return
            end if
            ! compute output EFT functions if needed:
            call output( num_eq, i+1, t2, y )

        end do

        call output( num_eq, self%EFTOmega%num_points, t2, y )
        success = .True.
        return

    contains

        ! ---------------------------------------------------------------------------------------------
        !> Subroutine that computes y' given y for the background of designer f(R)
        subroutine derivs( num_eq, x, y, ydot )

            implicit none

            integer , intent(in)                     :: num_eq !< number of equations in the ODE system
            real(dl), intent(in)                     :: x      !< time at which the derivatives of the system are computed
            real(dl), intent(in) , dimension(num_eq) :: y      !< input status of the system
            real(dl), intent(out), dimension(num_eq) :: ydot   !< value of the derivative at time x

            real(dl) :: a

            ! convert x in a:
            a = Exp(x)

            ! Get the equation of motion:
            ydot(1) = 1._dl/a*( exp(-self%alpha*a**self%b ) -1._dl -y(1) )

        end subroutine

        ! ---------------------------------------------------------------------------------------------
        !> Subroutine that computes the Jacobian of the system. Now a dummy function.
        !! Implementing it might increase performances.
        subroutine jacobian( num_eq, x, y, ml, mu, pd, nrowpd )

            implicit none

            integer                            :: num_eq !< number of components of the Jacobian
            integer                            :: ml     !< ignored
            integer                            :: mu     !< ignored
            integer                            :: nrowpd !< ignored
            real(dl)                           :: x      !< time at which the Jacobian is computed
            real(dl), dimension(num_eq)        :: y      !< input status of the system
            real(dl), dimension(nrowpd,num_eq) :: pd     !< output Jacobian

        end subroutine jacobian

        ! ---------------------------------------------------------------------------------------------
        !> Subroutine that takes the solution of the background f(R) equations and stores the values of the EFT functions.
        subroutine output( num_eq, ind, x, y )

            implicit none

            integer , intent(in)                     :: num_eq !< number of equations in the ODE system.
            integer , intent(in)                     :: ind    !< index of the EFT functions interpolation tables to fill.
            real(dl), intent(in)                     :: x      !< time at which the derivatives of the system are computed.
            real(dl), intent(in) , dimension(num_eq) :: y      !< input status of the system.

            real(dl) :: a

            ! 1) convert x in a:
            a = Exp(x)

            ! compute the EFT functions:
            self%EFTOmega%y(ind)    = y(1)
            self%EFTOmega%yp(ind)   = 1._dl/a*( exp(-self%alpha*a**self%b ) -1._dl -y(1))
            self%EFTOmega%ypp(ind)  = -1._dl/a*( self%alpha*self%b*a**(self%b-1)*exp(-self%alpha*a**self%b ) +2._dl*self%EFTOmega%yp(ind))
            self%EFTOmega%yppp(ind) = 1._dl/a*( -self%alpha*self%b*exp(-self%alpha*a**self%b )*( (self%b-1)*a**(self%b-2) &
                                    &-a**(self%b-1)*( self%alpha*self%b*a**(self%b-1))) -3._dl*self%EFTOmega%ypp(ind))

        end subroutine

    end subroutine EFTCAMBDesignerH0SolveDesignerEquations


    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that computes the number of parameters of the model.
    subroutine EFTCAMBDesH0ComputeParametersNumber( self )

        implicit none

        class(EFTCAMB_des_H0)  :: self   !< the base class

        self%parameter_number = 2

    end subroutine EFTCAMBDesH0ComputeParametersNumber

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that prints on the screen feedback information about the model.
    subroutine EFTCAMBDesH0Feedback( self, print_params )

        implicit none

        class(EFTCAMB_des_H0)       :: self         !< the base class
        logical, optional           :: print_params !< optional flag that decised whether to print numerical values
                                                    !! of the parameters.

        write(*,*)
        write(*,'(a,a)')    '   Model               =  ', self%name
        write(*,'(a,I3)')   '   Number of params    ='  , self%parameter_number
        ! print model functions informations:
        if ( self%EFTwDE /= 0 ) then
            write(*,*)
            write(*,'(a,I3)')  '   EFTwDE              =', self%EFTwDE
        end if
        if (self%alpha .ne. 0._dl) then
          write(*,*)
          print*, '   alpha(not free)     =', self%alpha
          print*, '   b                   =', self%b
          print*, '   c                   =', self%c
        end if

        call self%DesH0wDE%feedback( .false. )

    end subroutine EFTCAMBDesH0Feedback

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that returns the i-th parameter name of the model
    subroutine EFTCAMBDesH0ParameterNames( self, i, name )

        implicit none

        class(EFTCAMB_des_H0)       :: self   !< the base class
        integer     , intent(in)    :: i      !< the index of the parameter
        character(*), intent(out)   :: name   !< the output name of the i-th parameter

        ! check validity of input:
        if ( i<=0 .or. i>self%parameter_number ) then
            write(*,'(a,I3)') 'EFTCAMB error: no parameter corresponding to number ', i
            write(*,'(a,I3)') 'Total number of parameters is ', self%parameter_number
            call MpiStop('EFTCAMB error')
      else if ( i==1 ) then
            name = TRIM('b')
            return
        else if ( i==2 ) then
            name = TRIM('c')
            return
        end if

    end subroutine EFTCAMBDesH0ParameterNames

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that returns the i-th parameter name of the model
    subroutine EFTCAMBDesH0ParameterNamesLatex( self, i, latexname )

        implicit none

        class(EFTCAMB_des_H0) :: self       !< the base class
        integer     , intent(in)    :: i         !< The index of the parameter
        character(*), intent(out)   :: latexname !< the output latex name of the i-th parameter

        ! check validity of input:
        if ( i<=0 .or. i>self%parameter_number ) then
            write(*,'(a,I3)') 'EFTCAMB error: no parameter corresponding to number ', i
            write(*,'(a,I3)') 'Total number of parameters is ', self%parameter_number
            call MpiStop('EFTCAMB error')
       else if ( i==1 ) then
            latexname = TRIM('b')
            return
       else if ( i==2 ) then
            latexname = TRIM('c')
           return
       end if

    end subroutine EFTCAMBDesH0ParameterNamesLatex

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that returns the i-th parameter name of the model
    subroutine EFTCAMBDesH0ParameterValues( self, i, value )

        implicit none

        class(EFTCAMB_des_H0) :: self   !< the base class
        integer , intent(in)        :: i     !< The index of the parameter
        real(dl), intent(out)       :: value !< the output value of the i-th parameter

        ! check validity of input:
        if ( i<=0 .or. i>self%parameter_number ) then
            write(*,'(a,I3)') 'EFTCAMB error: no parameter corresponding to number ', i
            write(*,'(a,I3)') 'Total number of parameters is ', self%parameter_number
            call MpiStop('EFTCAMB error')
       else if ( i==1 ) then
            value = self%b
            return
       else if ( i==2 ) then
            value = self%c
            return
       end if

    end subroutine EFTCAMBDesH0ParameterValues

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that computes the value of the background EFT functions at a given time.
    subroutine EFTCAMBDesH0BackgroundEFTFunctions( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                        :: self          !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        real(dl) :: ab,ac,al2, gamma1,gamma2, b, OV,OP, OPP, OPPP

        real(dl) :: x, mu
        integer  :: ind

        ! ab  = a**self%b
        ! ac  = a**self%c
        ! al2 = self%alpha**2
        b = self%b

        x   = log(a)

        if(a==0_dl)return

        if (x<1.E-8_dl) then
        ! if (x<self%x_initial) then
          eft_cache%EFTOmegaV    = 0._dl
          eft_cache%EFTOmegaP    = 0._dl
          eft_cache%EFTOmegaPP   = 0._dl
          eft_cache%EFTOmegaPPP  = 0._dl
          return
        end if
        ! if (x>self%x_final ) return
        !
        ! call self%EFTOmega%precompute(x, ind, mu )

        ! eft_cache%EFTOmegaV    = self%EFTOmega%value( x, index=ind, coeff=mu )
        ! eft_cache%EFTOmegaP    = self%EFTOmega%first_derivative( x, index=ind, coeff=mu )
        ! eft_cache%EFTOmegaPP   = self%EFTOmega%second_derivative( x, index=ind, coeff=mu )
        ! eft_cache%EFTOmegaPPP  = self%EFTOmega%third_derivative( x, index=ind, coeff=mu )

        gamma1 = gammq(1._dl/self%b,a**self%b*self%alpha)*exp(gammln(1._dl/self%b))
        gamma2 = gammq(1/b,self%alpha/exp(18.420680743952367*b))*exp(gammln(1._dl/self%b))

        OV = -1. + 1.e-8/a - (1.*gamma1)/(b*(a**b*self%alpha)**(1/b)) +(1.e-8*gamma2)/(a*b*(self%alpha/exp(18.420680743952367*b))**(1./b))
        ! write (11,*)a, OV, gamma1
        OP =-1.e-8/a**2 + 1./(a*exp(a**b*self%alpha)) + (1.*a**(-1 + b)*self%alpha*(a**b*self%alpha)**(-1 - 1/b)*gamma1)/b &
            &-(1.e-8*gamma2)/(a**2*b*(self%alpha/exp(18.420680743952367*b))**(1./b))

        OPP = 2.e-8/a**3 - 2./(a**2*exp(a**b*self%alpha)) - (1.*a**(-2 + b)*b*self%alpha)/exp(a**b*self%alpha) &
            &+1.*a**(-2 + 2*b)*(-1 - 1/b)*self%alpha**2*(a**b*self%alpha)**(-2 - 1/b)*gamma1 +(1.*a**(-2 + b)*(-1 &
            &+ b)*self%alpha*(a**b*self%alpha)**(-1 - 1/b)*gamma1)/b +(2.e-8*gamma2)/(a**3*b*(self%alpha/exp(18.420680743952367*b))**(1./b))

        OPPP = -6.000000000000001e-8/a**4 + 4./(a**3*exp(a**b*self%alpha)) - (1.*(-1 + b))/(a**3*exp(a**b*self%alpha)) &
            &- (1.*(-1 - 1/b)*b)/(a**3*exp(a**b*self%alpha)) +(2.*a**(-3 + b)*b*self%alpha)/exp(a**b*self%alpha) &
            &- (1.*a**(-3 + b)*(-2 + b)*b*self%alpha)/exp(a**b*self%alpha) + (1.*a**(-3 + 2*b)*b**2*self%alpha**2)/exp(a**b*self%alpha) &
            &+1.*a**(-3 + 3*b)*(-2 - 1/b)*(-1 - 1/b)*b*self%alpha**3*(a**b*self%alpha)**(-3 - 1/b)*gamma1 +1.*a**(-3 + 2*b)*(-1 &
            &- 1/b)*(-1 + b)*self%alpha**2*(a**b*self%alpha)**(-2 - 1/b)*gamma1 +1.*a**(-3 + 2*b)*(-1 - 1/b)*(-2 &
            &+ 2*b)*self%alpha**2*(a**b*self%alpha)**(-2 - 1/b)*gamma1 +(1.*a**(-3 + b)*(-2 + b)*(-1 + b)*self%alpha*(a**b*self%alpha)**(-1 &
            &- 1/b)*gamma1)/b -(6.000000000000001e-8*gamma2)/(a**4*b*(self%alpha/exp(18.420680743952367*b))**(1./b))

        eft_cache%EFTOmegaV    = OV
        eft_cache%EFTOmegaP    = OP
        eft_cache%EFTOmegaPP   = OPP
        eft_cache%EFTOmegaPPP  = OPPP

        ! write (11,*)a, self%DesH0wDE%value(a,eft_cache,eft_par_cache),self%DesH0wDE%first_derivative(a,eft_cache,eft_par_cache)
        ! write (12,*)a, self%DesH0wDE%second_derivative(a,eft_cache,eft_par_cache),self%DesH0wDE%third_derivative(a,eft_cache,eft_par_cache)
        ! write (13,*)a, self%DesH0wDE%integral(a,eft_cache,eft_par_cache)
        !
        ! gamma1 = gammq(1._dl/self%b,a**self%b*al2)*exp(gammln(1._dl/self%b))
        !
        ! eft_cache%EFTOmegaV    = -( al2*ab )**(-1._dl/self%b)*gamma1/self%b-1._dl
        ! eft_cache%EFTOmegaP    = (-1._dl + exp(-(ab*al2)) + (ab*al2*(a**self%b*al2)**(-1._dl - 1._dl/self%b)*gamma1)/self%b&
        !  &-gamma1/(self%b*(ab*al2)**(1._dl/self%b)))/a - (-a - (a*gamma1)/(self%b*(ab*al2)**(1._dl/self%b)))/a**2
        !
        ! eft_cache%EFTOmegaPP   = (-((a**(-1._dl + b)*b*al2)/exp(ab*al2)) + a**(-1._dl + 2._dl*b)*(-1._dl - 1._dl/b)*al2**2*(ab*al2)**(-2._dl - 1._dl/b)*gamma1 &
        !     &+ a**(-1._dl + b)*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1 +(a**(-1._dl + b)*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1)/b)/a -(2._dl*(-1._dl + exp(-(ab*al2)) &
        !     &+ (ab*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1)/b - gamma1/(b*(ab*al2)**(1/b))))/a**2._dl + (2._dl*(-a - (a*gamma1)/(b*(ab*al2)**(1._dl/b))))/a**3
        !
        ! eft_cache%EFTOmegaPPP  =   (-3._dl*(-((a**(-1._dl + b)*b*al2)/exp(ab*al2)) +a**(-1._dl + 2._dl*b)*(-1._dl - 1._dl/b)*al2**2*(ab*al2)**(-2._dl - 1._dl/b)*gamma1 &
        !     &+a**(-1._dl + b)*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1 +(a**(-1._dl + b)*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1)/b))/a**2 +(-(1._dl/(a**2*exp(ab*al2))) &
        !     &- b/(a**2*exp(ab*al2)) -((-1._dl - 1._dl/b)*b)/(a**2*exp(ab*al2)) -(a**(-2 + b)*(-1._dl + b)*b*al2)/exp(ab*al2) + (a**(-2 + 2*b)&
        !     &*b**2*al2**2)/exp(ab*al2) +a**(-2 + 3*b)*(-2._dl -1._dl/b)*(-1._dl - 1._dl/b)*b*al2**3*(ab*al2)**(-3._dl - 1._dl/b)*gamma1 +a**(-2._dl + 2._dl*b)*(-1._dl - 1._dl/b)&
        !     &*al2**2*(ab*al2)**(-2 - 1/b)*gamma1 +a**(-2 + 2*b)*(-1._dl - 1._dl/b)*b*al2**2*(ab*al2)**(-2 - 1/b)*gamma1 +a**(-2 + 2*b)*(-1._dl -1._dl/b)&
        !     &*(-1._dl + 2._dl*b)*al2**2*(ab*al2)**(-2 - 1/b)*gamma1 +a**(-2 + b)*(-1._dl + b)*al2*(ab*al2)**(-1 - 1/b)*gamma1 &
        !     &+(a**(-2 + b)*(-1._dl + b)*al2*(ab*al2)**(-1._dl - 1._dl/b)*gamma1)/b)/a +(6._dl*(-1._dl + exp(-(ab*al2)) &
        !     &+ (ab*al2*(ab*al2)**(-1 - 1/b)*gamma1)/b -gamma1/(b*(ab*al2)**(1/b))))/a**3 - (6._dl*(-a -(a*gamma1)/(b*(ab*al2)**(1/b))))/a**4

        eft_cache%EFTc         = ( eft_cache%adotoa**2 - eft_cache%Hdot )*( eft_cache%EFTOmegaV + 0.5_dl*a*eft_cache%EFTOmegaP ) &
            & -0.5_dl*( a*eft_cache%adotoa )**2*eft_cache%EFTOmegaPP&
            & +0.5_dl*eft_cache%grhov_t*( 1._dl+ self%DesH0wDE%value(a,eft_cache,eft_par_cache) )
        eft_cache%EFTLambda    = -eft_cache%EFTOmegaV*( 2._dl*eft_cache%Hdot +eft_cache%adotoa**2 ) &
            & -a*eft_cache%EFTOmegaP*( 2._dl*eft_cache%adotoa**2 + eft_cache%Hdot ) &
            & -( a*eft_cache%adotoa )**2*eft_cache%EFTOmegaPP &
            & +self%DesH0wDE%value(a,eft_cache,eft_par_cache)*eft_cache%grhov_t
        eft_cache%EFTcdot      = +0.5_dl*eft_cache%adotoa*eft_cache%grhov_t*( -3._dl*(1._dl &
            &+self%DesH0wDE%value(a,eft_cache,eft_par_cache))**2 + a*self%DesH0wDE%first_derivative(a,eft_cache,eft_par_cache) ) &
            & -eft_cache%EFTOmegaV*( eft_cache%Hdotdot -4._dl*eft_cache%adotoa*eft_cache%Hdot +2._dl*eft_cache%adotoa**3 ) &
            & +0.5_dl*a*eft_cache%EFTOmegaP*( -eft_cache%Hdotdot +eft_cache%adotoa*eft_cache%Hdot +eft_cache%adotoa**3) &
            & +0.5_dl*a**2*eft_cache%adotoa*eft_cache%EFTOmegaPP*( eft_cache%adotoa**2 -3._dl*eft_cache%Hdot ) &
            & -0.5_dl*(a*eft_cache%adotoa)**3*eft_cache%EFTOmegaPPP
        eft_cache%EFTLambdadot = -2._dl*eft_cache%EFTOmegaV*( eft_cache%Hdotdot -eft_cache%adotoa*eft_cache%Hdot -eft_cache%adotoa**3 ) &
            & -a*eft_cache%EFTOmegaP*( +eft_cache%Hdotdot +5._dl*eft_cache%adotoa*eft_cache%Hdot -eft_cache%adotoa**3  ) &
            & -a**2*eft_cache%EFTOmegaPP*eft_cache%adotoa*( +2._dl*eft_cache%adotoa**2 +3._dl*eft_cache%Hdot )&
            & -(a*eft_cache%adotoa)**3*eft_cache%EFTOmegaPPP &
            & +eft_cache%grhov_t*eft_cache%adotoa*( a*self%DesH0wDE%first_derivative(a,eft_cache,eft_par_cache) &
            &-3._dl*self%DesH0wDE%value(a,eft_cache,eft_par_cache)*(1._dl +self%DesH0wDE%value(a,eft_cache,eft_par_cache) ))

    end subroutine EFTCAMBDesH0BackgroundEFTFunctions

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that computes the value of the background EFT functions at a given time.
    subroutine EFTCAMBDesH0SecondOrderEFTFunctions( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                        :: self          !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        eft_cache%EFTGamma1V  = 0._dl
        eft_cache%EFTGamma1P  = 0._dl
        eft_cache%EFTGamma2V  = 0._dl
        eft_cache%EFTGamma2P  = 0._dl
        eft_cache%EFTGamma3V  = 0._dl
        eft_cache%EFTGamma3P  = 0._dl
        eft_cache%EFTGamma4V  = 0._dl
        eft_cache%EFTGamma4P  = 0._dl
        eft_cache%EFTGamma4PP = 0._dl
        eft_cache%EFTGamma5V  = 0._dl
        eft_cache%EFTGamma5P  = 0._dl
        eft_cache%EFTGamma6V  = 0._dl
        eft_cache%EFTGamma6P  = 0._dl

    end subroutine EFTCAMBDesH0SecondOrderEFTFunctions

    ! ---------------------------------------------------------------------------------------------
    !> Function that computes dtauda = 1/sqrt(a^2H^2).For pure EFT std this has to be overridden
    !! for performance reasons.
    function EFTCAMBDesH0ComputeDtauda( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                   :: self          !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        real(dl) :: EFTCAMBDesH0ComputeDtauda               !< the output dtauda

        real(dl) :: temp

        temp = eft_cache%grhoa2 +eft_par_cache%grhov*a*a*self%DesH0wDE%integral(a,eft_cache ,eft_par_cache)
        EFTCAMBDesH0ComputeDtauda = sqrt(3/temp)

    end function EFTCAMBDesH0ComputeDtauda

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that computes adotoa = H.
    subroutine EFTCAMBDesH0ComputeAdotoa( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                       :: self           !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        eft_cache%grhov_t = eft_par_cache%grhov*self%DesH0wDE%integral(a,eft_cache ,eft_par_cache)
        eft_cache%adotoa  = sqrt( ( eft_cache%grhom_t +eft_cache%grhov_t )/3._dl )

    end subroutine EFTCAMBDesH0ComputeAdotoa

    ! ---------------------------------------------------------------------------------------------
    !> Subroutine that computes the two derivatives wrt conformal time of H.
    subroutine EFTCAMBDesH0ComputeHubbleDer( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                   :: self          !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        eft_cache%gpiv_t  = self%DesH0wDE%value(a,eft_cache,eft_par_cache)*eft_cache%grhov_t
        eft_cache%Hdot    = -0.5_dl*( eft_cache%adotoa**2 +eft_cache%gpresm_t +eft_cache%gpiv_t )
        eft_cache%Hdotdot = eft_cache%adotoa*( ( eft_cache%grhob_t +eft_cache%grhoc_t)/6._dl +2._dl*( eft_cache%grhor_t +eft_cache%grhog_t)/3._dl ) &
            & +eft_cache%adotoa*eft_cache%grhov_t*( 1._dl/6._dl +self%DesH0wDE%value(a,eft_cache,eft_par_cache) &
            &+1.5_dl*self%DesH0wDE%value(a,eft_cache,eft_par_cache)**2 -0.5_dl*a*self%DesH0wDE%first_derivative(a,eft_cache,eft_par_cache) ) &
            & +eft_cache%adotoa*eft_cache%grhonu_tot/6._dl -0.5_dl*eft_cache%adotoa*eft_cache%gpinu_tot -0.5_dl*eft_cache%gpinudot_tot

     end subroutine EFTCAMBDesH0ComputeHubbleDer

    ! ---------------------------------------------------------------------------------------------
    !> Function that computes model specific stability requirements.
    function EFTCAMBDesH0AdditionalModelStability( self, a, eft_par_cache, eft_cache )

        implicit none

        class(EFTCAMB_des_H0)                   :: self          !< the base class
        real(dl), intent(in)                         :: a             !< the input scale factor.
        type(EFTCAMB_parameter_cache), intent(inout) :: eft_par_cache !< the EFTCAMB parameter cache that contains all the physical parameters.
        type(EFTCAMB_timestep_cache ), intent(inout) :: eft_cache     !< the EFTCAMB timestep cache that contains all the physical values.

        logical :: EFTCAMBDesH0AdditionalModelStability          !< the return value of the stability computation. True if the model specific stability criteria are met, false otherwise.

        EFTCAMBDesH0AdditionalModelStability = .True.
        ! if ( self%DesH0wDE%value(a,eft_cache,eft_par_cache) > -1._dl/3._dl ) EFTCAMBDesH0AdditionalModelStability = .False.

    end function EFTCAMBDesH0AdditionalModelStability

    ! ---------------------------------------------------------------------------------------------

end module EFTCAMB_designer_H0

!----------------------------------------------------------------------------------------
